﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class CipherNumberEqualityComparer : EqualityComparer<FullNumber>
    {
        public override bool Equals(FullNumber x, FullNumber y)
        {
            if(x == null || y == null)
            {
                // Ниже происходит что-то странное. Должно быть как-то так:
                // return x == null && y == null;
                if (x == null && y == null)
                {
                    return true;
                }
                {
                    return false;
                }
            }
            
            // Строки не принято сравнивать через ==. Нужно через статику: string.Equals(first, second, StringComparison.Xxxxxxx)
            return (x.Cipher == y.Cipher) && (x.Number == y.Number);
        }

        
        public override int GetHashCode(FullNumber number)
        {
            var h = 11;
            h = 31 * h + number.Cipher.GetHashCode(); // А если Cipher == null?
            h = 31 * h + number.Number.GetHashCode();
            return h;
        }
    }
}
