﻿
namespace Lab5Algor
{
	public class MenuCommand
	{
		private string _title;
		private IExecutable _executable;

		public MenuCommand(string title, IExecutable executable)
		{
			_title = title;
			_executable = executable;
		}

		public string Title
		{
			get { return _title; }
		}

		public void Execute()
		{
			_executable.Execute();
		}
	}
}
