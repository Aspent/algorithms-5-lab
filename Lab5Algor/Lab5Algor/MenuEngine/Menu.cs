﻿
using System;
using System.Collections.Generic;

namespace Lab5Algor
{
	public class Menu
	{
		private readonly List<MenuCommand> _commands = new List<MenuCommand>();

		public void AddCommand(string title, IExecutable executable)
		{
			_commands.Add(new MenuCommand(title, executable));
		}

		public void Run()
		{
			while (true)
			{
				PrintCommands();
				var command = ReadCommand();
                while (command < 1 || command > _commands.Count)
                {
                    Console.WriteLine("Команды с таким номером не существует");
                    Console.WriteLine("Попробуйте еще раз");
                    command = ReadCommand();
                }
				ExecuteCommand(command);
			}
		}

		private void ExecuteCommand(int command)
		{
			_commands[command - 1].Execute();
		}

		private int ReadCommand()
		{
            Console.Write("Введите номер команды: ");
            int commandNumber;
            while (!int.TryParse(Console.ReadLine(), out commandNumber))
            {
                Console.WriteLine("Неверный ввод");
                Console.WriteLine("Попробуйте еще раз");
            }

			return commandNumber;
		}

		private void PrintCommands()
		{
			Console.WriteLine();
			var number = 1;
			foreach (var command in _commands)
			{
				Console.WriteLine("{0} - {1}", number, command.Title);
				++number;
			}
		}
	}
}
