﻿
namespace Lab5Algor
{
	public interface IExecutable
	{
		void Execute();
	}
}
