﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class DocumentsRepository
    {
        private readonly List<Document> _repository = new List<Document>();
        private readonly Dictionary<FullNumber, List<Document>> _defaultOrderDictionary
            = new Dictionary<FullNumber, List<Document>>();
        private readonly Dictionary<FullNumber, List<Document>> _cipherNumberOrderDictionary
            = new Dictionary<FullNumber, List<Document>>(new CipherNumberEqualityComparer());
        private readonly Dictionary<string, List<Document>> _responsibleOrderDictionary
            = new Dictionary<string, List<Document>>();
      
        public void Add(Document document)
        {
            _repository.Add(document);
            if (!_defaultOrderDictionary.ContainsKey(document.FullNumber))
            {
                _defaultOrderDictionary[document.FullNumber] = new List<Document>();
            }
            _defaultOrderDictionary[document.FullNumber].Add(document);

            if (!_cipherNumberOrderDictionary.ContainsKey(document.FullNumber))
            {
                _cipherNumberOrderDictionary[document.FullNumber] = new List<Document>();
            }
            _cipherNumberOrderDictionary[document.FullNumber].Add(document);

            if (!_responsibleOrderDictionary.ContainsKey(document.NameOfResponsible))
            {
                _responsibleOrderDictionary[document.NameOfResponsible] = new List<Document>();
            }
            _responsibleOrderDictionary[document.NameOfResponsible].Add(document);
        }

        public IEnumerable<Document> GetAll()
        {
            return _repository;
        }

        public IEnumerable<Document> GetAllOrdered()
        {
            // можно так: return _repository.OrderBy(x => x);
            var sortedRepository = _repository.ToList();
            sortedRepository.Sort();
            return sortedRepository;
        }

        public IEnumerable<Document> GetAllOrdered(Comparer<Document> order)
        {
            // можно так: return _repository.OrderBy(x => x, order);
            // Но вообще предполагалось, что будет метод GetAllOrderedByXxxxxx, а компарер будет использоваться здесь, а не передаваться снаружи
            var sortedRepository = _repository.ToList();
            sortedRepository.Sort(order);
            return sortedRepository;
        }

        public IEnumerable<Document> GetByFullNumber(FullNumber key)
        {
            List<Document> documents;
            _defaultOrderDictionary.TryGetValue(key, out documents);
            return documents;
        }

        public IEnumerable<Document> GetByCipherNumber(string cipher, int number)
        {
            var key = new FullNumber(cipher, number, new DateTime());
            List<Document> documents;
            _cipherNumberOrderDictionary.TryGetValue(key, out documents);
            return documents;
        }

        public IEnumerable<Document> GetByResponsible(string name)
        {
            List<Document> documents;
            _responsibleOrderDictionary.TryGetValue(name, out documents);
            return documents;
        }


    }
}
