﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    // Нужно использовать в первую очередь типизированный IComparable<T>. А в идеале, типизированный и нетипизированный: IComparable, IComparable<T>. Причем в последнем случае, IComparable реализовывать явно.
    // В этой лабе достаточно будет IComparable<Document>
    class Document : IComparable
    {
        private readonly FullNumber _fullNumber;
        private readonly string _title;
        private readonly string _status;
        private readonly string _nameOfResponsible;
        private readonly DateTime _creationDate;

        // nameOfResponsible -- проще надо: responsibleName
        public Document(FullNumber fullNumber, string title, string status,
            string nameOfResponsible, DateTime creationDate)
        {
            _fullNumber = fullNumber;
            _title = title;
            _status = status;
            _nameOfResponsible = nameOfResponsible;
            _creationDate = creationDate;
        }

        // если у типа есть синоним в C#, то мы обычно пользуемся им: object (с маленькой буквы)
        public int CompareTo(Object obj)
        {
            if(obj == null )
            {
                if(this == null) // предполагается, что такого не бывает :) (Обычно при попытке вызвать что-то на null вылетает NullReferenceException)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            Document otherDocument = obj as Document;
            if(otherDocument != null)
            {
                // Если строка может содержать текст на человеческом языке, то Ordinal использовать неправильно. Нужно сравнивать хотя бы с использованием CurrentCulture.
                // Можно посмотреть что-то типа string.Compare(first, second, StringComparison.CurrentCulture)
                return string.CompareOrdinal(_title, otherDocument._title);             
            }
            else
            {
                throw new ArgumentException("Argument isn't document");
            }
        }

        // Проперти обычно пишут выше всяких-разных методов
        public FullNumber FullNumber
        {
            get { return _fullNumber; }
        }

        public string Title
        {
            get { return _title; }
        }

        public string Status
        {
            get { return _status; }
        }

        public string NameOfResponsible
        {
            get { return _nameOfResponsible; }
        }

        public DateTime CreationDate
        {
            get { return _creationDate; }
        }
    }
}
