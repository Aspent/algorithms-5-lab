﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class FullNumber : IEquatable<FullNumber>
    {
        private readonly string _cipher;
        private readonly int _number;
        private readonly DateTime _signingDate;

        public FullNumber(string cipher, int number, DateTime signingDate)
        {
            _cipher = cipher;
            _number = number;
            _signingDate = signingDate;
        }

        public bool Equals(FullNumber other)
        {
            if(other == null)
            {
                return false;
            }
            // 1. Equals(FullNumber) и Equals(object) содержат дублирующуюся логику сравнения
            // 2. Сейчас Equals(FullNumber) и Equals(object) -- сравнивают по-разному: если в object-версию передать наследника FullNumber, то она точно вернет false, а FullNumber-версия может вернуть и true.            
            return (_cipher == other._cipher) && (_number == other._number)
                && (_signingDate == other._signingDate);
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }
            if(ReferenceEquals(this, obj))
            {
                return true;
            }
            if(GetType() != obj.GetType())
            {
                return false;
            }
            
            var other = (FullNumber)obj;
            return (_cipher == other._cipher) && (_number == other._number)
                && (_signingDate == other._signingDate);
        }

        public override int GetHashCode()
        {
            var h = 11;
            h = 31 * h + _cipher.GetHashCode(); // а если _cipher == null? Все упадет, но GetHashCode падать не должен
            h = 31 * h + _number.GetHashCode();
            h = 31 * h + _signingDate.GetHashCode();
            return h;
        }

        public string Cipher
        {
            get { return _cipher; }
        }

        public int Number
        {
            get { return _number; }
        }

        public DateTime SigningDate
        {
            get { return _signingDate; }
        }
    }
}
