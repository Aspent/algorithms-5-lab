﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class Program
    {
        static void Main(string[] args)
        {           
            var documents = new DocumentsRepository();
            documents.Add(new Document(new FullNumber("1200", 1, new DateTime(1995, 8, 12)), 
                "Табель о рангах", "Утверждается", "Петров А.В.", new DateTime(1995, 7, 30)));
            documents.Add(new Document(new FullNumber("7700", 5, new DateTime(2015, 9, 6)),
                "Судебник", "Проект", "Васильев П.И.", new DateTime(2015, 9, 5)));
            documents.Add(new Document(new FullNumber("3600", 4, new DateTime(2014, 2, 27)),
                "Постановление № 144", "Дорабатывается", "Сидоров Н.П.", new DateTime(2013, 9, 26)));
            documents.Add(new Document(new FullNumber("5400", 7, new DateTime(1963, 7, 7)),
                "Закон о реке", "Утвержден", "Орехова Г.Р.", new DateTime(1961, 11, 9)));
            documents.Add(new Document(new FullNumber("7200", 3, new DateTime(2002, 10, 18)),
                "Договор о поставке", "Проект", "Якунин Ю.И.", new DateTime(2000, 1, 4)));
            documents.Add(new Document(new FullNumber("2456", 6, new DateTime(1938, 5, 19)),
                "Акт о списании", "Утвержден", "Арбузова И.Н.", new DateTime(1936, 12, 1)));
            documents.Add(new Document(new FullNumber("5411", 2, new DateTime(2013, 2, 21)),
                "Заявление №2", "Утверждается", "Иванов Е.К.", new DateTime(2012, 4, 13)));
            documents.Add(new Document(new FullNumber("2456", 6, new DateTime(1878, 6, 3)),
                "Федералный закон", "Дорабатывается", "Павлов Д.Ф.", new DateTime(1896, 10, 4)));
            documents.Add(new Document(new FullNumber("8504", 3, new DateTime(2007, 9, 9)),
                "Грамота", "Утвержден", "Якунин Ю.И.", new DateTime(2007, 1, 10)));

            var menu = new Menu();
            menu.AddCommand("Вывести список документов", new ShowDocumentsExecutable(documents.GetAll()));
            menu.AddCommand("Вывести список документов, отсортированный по названию",
                new ShowDocumentsExecutable(documents.GetAllOrdered()));
            menu.AddCommand("Вывести список документов, отсортированный по убыванию даты",
                new ShowDocumentsExecutable(documents.GetAllOrdered(new CreationDateComparer())));
            menu.AddCommand("Вывести список документов, отсортированный по ФИО ответственного",
                new ShowDocumentsExecutable(documents.GetAllOrdered(new ResponsibleComparer())));
            menu.AddCommand("Найти по документ по полному номеру",
                new FindByFullNumberExecutable(documents));
            menu.AddCommand("Найти по документ по шифру и номеру",
                new FindByCipherNumberExecutable(documents));
            menu.AddCommand("Найти по документ по ФИО ответственного",
                new FindByResponsibleExecutable(documents));
            menu.AddCommand("Выход", new ExitExecutable());
            menu.Run();
        }
    }
}
