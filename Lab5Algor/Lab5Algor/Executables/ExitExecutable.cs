﻿
using System;
using System.Net.Mime;

namespace Lab5Algor
{
	public class ExitExecutable : IExecutable
	{
		public void Execute()
		{
			Environment.Exit(0);
		}
	}
}
