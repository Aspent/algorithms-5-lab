﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class FindByFullNumberExecutable : IExecutable
    {
        private readonly DocumentsRepository _repository;

        public FindByFullNumberExecutable(DocumentsRepository repository)
        {
            _repository = repository;
        }

        public void Execute()
        {
            Console.WriteLine("Введите шифр");
            var cipher = Console.ReadLine();
            while (cipher == "")
            {
                Console.WriteLine("Шифр не введен");
                Console.WriteLine("Попробуйте еще раз");
                cipher = Console.ReadLine();
            }
            Console.WriteLine("Введите номер");
            int number;
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Неверный ввод");
                Console.WriteLine("Попробуйте еще раз");
            }

            Console.WriteLine("Введите дату подписания");
            DateTime signingDate;
            while (!DateTime.TryParse(Console.ReadLine(), out signingDate))
            {
                Console.WriteLine("Неверный ввод");
                Console.WriteLine("Попробуйте еще раз");
            }

            var documents = _repository.GetByFullNumber(new FullNumber(cipher, number, signingDate));
            Console.WriteLine();
            if (documents == null)
            {
                Console.WriteLine("Нет элементов с таким ключом");
            }
            else
            {
                new ShowDocumentsExecutable(documents).Execute();
            }
        }
    }
}
