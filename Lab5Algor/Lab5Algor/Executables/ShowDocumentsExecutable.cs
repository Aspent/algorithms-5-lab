﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class ShowDocumentsExecutable : IExecutable
    {
        private readonly IEnumerable<Document> _repository;

        public ShowDocumentsExecutable(IEnumerable<Document> repository)
        {
            _repository = repository;
        }

        public void Execute()
        {

            foreach(var t in _repository)
            {
                Console.WriteLine("Полный номер: {0}, {1}, {2}", t.FullNumber.Cipher, t.FullNumber.Number,
                    t.FullNumber.SigningDate);
                Console.WriteLine("Наименование: {0}", t.Title);
                Console.WriteLine("Статус: {0}", t.Status);
                Console.WriteLine("Ответственный: {0}", t.NameOfResponsible);
                Console.WriteLine("Дата создания: {0}", t.CreationDate);
                Console.WriteLine();
            }
        }
    }
}
