﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class FindByResponsibleExecutable : IExecutable
    {
        private readonly DocumentsRepository _repository;

        public FindByResponsibleExecutable(DocumentsRepository repository)
        {
            _repository = repository;
        }

        public void Execute()
        {
            Console.WriteLine("Введите ФИО ответственного");
            var responsible = Console.ReadLine();
            while (responsible == "")
            {
                Console.WriteLine("ФИО ответственного не введено");
                Console.WriteLine("Попробуйте еще раз");
                responsible = Console.ReadLine();
            }
            var documents = _repository.GetByResponsible(responsible);
            if (documents.ToList().Count == 0)
            {
                Console.WriteLine("Нет элементов с таким ключом");
            }
            else
            {
                new ShowDocumentsExecutable(documents).Execute();
            }
        }
    }
}
