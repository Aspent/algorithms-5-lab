﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    // Два почти одинаковых сравнения -- это неинтересно. Нужно проявить творческий подход и придумать что-то еще :)
    // По заголовку посравнивать, и/или по статусу, или еще как.
    class ResponsibleComparer : Comparer<Document>
    {
        public override int Compare(Document x, Document y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }
            if (string.CompareOrdinal(x.NameOfResponsible, y.NameOfResponsible) != 0)
            {
                return string.CompareOrdinal(x.NameOfResponsible, y.NameOfResponsible);
            }
            else
            {
                return x.CreationDate.CompareTo(y.CreationDate);
            }
        }
    }
}
