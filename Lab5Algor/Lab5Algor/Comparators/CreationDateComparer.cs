﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5Algor
{
    class CreationDateComparer : Comparer<Document>
    {
        public override int Compare(Document x, Document y)
        {
            if(x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }  
            if(x.CreationDate.CompareTo(y.CreationDate) != 0)
            {
                return -(x.CreationDate.CompareTo(y.CreationDate));
            }
            else
            {
                // см. замечание в Document
                return string.CompareOrdinal(x.NameOfResponsible, y.NameOfResponsible);
            }            
        }
    }
}
